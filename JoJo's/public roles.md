```fix
Joinable roles. Use .join NAME to join a role
```
Available roles:

Part1 or PhantomBlood
Part2 or BattleTendency
Part3 or StardustCrusaders
Part4 or DiamondIsUnbreakable
Part5 or VentoAureo
Part6 or StoneOcean
Part7 or SteelBallRun
Part8 or JoJolion
Spoilers
Starboard