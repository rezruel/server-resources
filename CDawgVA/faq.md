Congratulations on finding <#344103613242802176>, the one stop shop for most of your easily answerable questions. In here we will post frequently asked questions, and the answers to them. Wondering how @Ayana#8911 in <#306556297635954688> works? Or what the `1 👀` role means? Then you’ve come to the right place!


```fix
How do I play music in #jukebox-bot?
```
Well, first off, connect to the JukeBox voice chat. Check out the commands by writing `=music`, alternatively `=m`, in <#306556297635954688>. To get started, add songs by writing `=m q p holla do it for the love`, you’ll get asked for which version you want to queue, pick one by replying either with `1`, `2`, or `3`. After that, write `=m p`, and the bot will start playing music for you.


```fix
Where do I post things? What are the channels for?
```
Let's break things down in a simple manner. <#179674710856957952> is our main chat, in here you can talk to others about most things. Games, anime, manga, music, k-pop, even (*sigh*) CDawgVA. :^)

<#187262140476817411> has been provided for those that want to join voice chats with others, but for some reason cannot use their microphone or voice, in here, people reply to the others in the voice chat, without causing confusion in the other chats. **However,** if you are in the Music Studio voice chat, kindly use <#188459500859162625> instead. For the Jukebox channel, please use <#306556297635954688>.

<#254025419437768705> is here for posting your non memes, foods, songs you like, and so on. The only things we'd rather have you post elsewhere are, anime posts (in <#180379116460048384>), memes and shitposts (in <#179677195634081793> except for text memes, we don't allow them), Black Butler (in <#190510241450950656>). We also prefer to have gaming related posts in <#197910803649789953>. **We don't allow advertising however.**
**For artists!** Please post your art in <#180741165497188352> instead, but only your own works!

<#179677195634081793> We briefly touched on this earlier, so to reiterate, this is the place of memes, shitposts, and content of otherwise little value :^) (Or the Connor memes, because he's our trashlord). However, do not post the cool dogs, or other text/ascii art/memes.

Interested in voice acting, got any burning questions, or seeking help with the craft in general? Then you'll find <#180421820527280137> to be the right place!

Are you a singer, or do you want to listen to the people in the Music Studio? Then kindly use the companion chat <#188459500859162625> for interaction, remember, it's a courtesy to the singers for you to be muted during their performance. Thus, we have a text chat for you to use while they're performing. :^)

As previously mentioned, <#306556297635954688> is for your JukeBox, music listening needs, queue up songs, and chat to the other listeners in there. Remember though, keep it wholesome and enjoyable for others, don't queue up multi hour songs, or super long playlists, everyone has something that they want to listen to, just like you.

*Writes a meta chat meta entry about a meta roleplay being meta roleplayed in the roleplay chat*...I mean what? Yes, we have a roleplay chat for you, it's right over in <#179969102868250624>.

**Awesome creative quarters!** Yes, we have an arts channel where you can show off the amazing artwork you've made, it's right on over in <#180741165497188352>. **Only your own pieces are allowed.**

Sebastian and Ciel comedy channel? Unfortunately, no, they're not a funny pair, but we do have a channel for the Black Butler fans <#190510241450950656>.

Anime? Manga? Why yes of course, step on in, come on up to the great, amazing, <#180379116460048384>!

Need advice on something for a school project, seeking help, need some comfort over a breakup, or something else? Then please stop on by <#256598483513901067>, where those that can, help those that need it.

Games? Games! **GAMES!** We all like games, yeah? Cool, we've got the toughest of the toughest, the roughest of the roughest, and Josh's (carrying CDawgVA in) League obsession, right in <#197910803649789953>!

Do you have an idea for CDawgVA's videos? Something to improve on or do with the server? Then please leave your idea in <#179721739968839680>.

Got an issue with Discord, your phone, internet, or anything else related to tech? Head on over to <#183978903403102208> with a description of your problem, and feel free to tag <@&282650496421068800> or <@&329341687463804929>.

In addition to these standard text channels, we have some exclusive ones. We have a chat for the community contributors, the Top Dawgs, where they can leave feedback or ideas directly for Connor to read. Super Patreon chat, where all of the patrons can hang out. And beautiful weebs chat where the Beautiful Weebs get together to chill. To gain access to these, you can either read the bottom section of <#339089718459236364>, or scroll down to the section about roles, donations, and more.


```fix
What are the different voice channels for?
```
Well the lounges (Lounge I, Lounge II and so on) are for general conversations. Pretty chill and relaxed, if you don’t have a mic available to you, but still want to talk to people in the same voice chat as you, use #mute-chat. The **Beautiful Weebs** and **Super Patreon Weebs** voice chats are for donators, again, for general conversations.

**JukeBox** is for our music bot, it also has its companion text chat, #jukebox-bot. **Music Studio**, come here to sing, and listen to others sing. Music Studio also has its companion text chat, #music-chat, here you can talk to the others in Music Studio (if you are in there yourself), you can keep track of the current list of singers, or sign up to take your turn, check the pinned messages in there. **Jam Studio** is for you instrumentalists out there, if you want to play an instrument, or casually hang around and talk to people playing music, join in there. **NOTE:** this channel has no companion text chat, if you need to be muted for some reason, use #mute-chat.

**Gaming 1** and 2, are you playing games? If so, please do not hang around in the regular lounges, grab your pals, and head to gaming, we expect rage over gaming moments in there, but not in the lounges.
For the **C A H** voice chat, this is only used by Cards Against Humanity players, just post your cards against humanity links in #share, and join this chat.

**Voice Acting**, this chat is for those that have voice acting related questions, whether it is in terms of the acting, equipment, editing, career, etc. **This is not where you do impressions of characters, or act out scenes as characters, for example, Ciel and Sebastian.** We have Impressions | RP for that instead.

**Impressions | RP**, want to act like Alois, Levi, or someone else for a bit? Or participate in a roleplay using oral communication? Swing on by here.
ᅠ
**Rant | Advice**, do you have anything you want to rant about? Pencil broke, heartbreak, family trouble? Come here to rant about it, and if you want, seek advice from others.

**Sleepy Weebs**, also known as the AFK room, if you’re going to be AFK for a longer period of time (10-15 minutes), please don’t hog a spot in the voice chat, if you are deemed AFK by a moderator, you’ll end up in here. This also goes for those that actually do fall asleep. If you find yourself waking up in here, you know why :^)

**Q&A**, a special event chat, with its own companion chat #q-n-a, in here we’ll hold Q&As with CDawgVA, or the moderators.


```fix
How do I get roles?
```
Well, for the most part, the roles that you can obtain are donator only. Either donating 5$ USD, or above, or a monthly pledge on Patreon will get you the donator roles. Donating 5$ gets you Beautiful Weebs, Patreons get Super Patreon Weebs, and after an entire month, they also get Beautiful Weebs.
Twitch donations can be done over on: <https://streamlabs.com/theonlycmc>
Patreon pledges can be sorted over on: <https://www.patreon.com/CDawgVA>

For the other roles, such as Top Dawgs, this requires contributing to CDawgVA himself, please do ask the moderators if there are any tasks available, it's very exclusive and limited. The Voice Actor role requires you to show proof of having been in multiple paid projects.

Oh and since some of you are probably wondering, we only take on mods when we open up mod applications, we don't forsee the need to do so in future, but we'll announce it in <#180016373005680640> when we do.


```fix
I'd like to report something, or, I’m really uncomfortable with something that is going on in this server (either text or voice), what should I do?
```
Contact one of the mods, tell them what’s going on, we can’t fix something we don’t know about. This can be something such as, you're uncomfortable with a meme, a topic, or something more serious.


```fix
I read on a server to copy paste this message to other servers? Should I be concerned?
```
Generally speaking, no, most of the copy paste requests you see are fake, the ones that go something like, “Discord is closing down”, “Watch out for X”, etc, are fake chainmail copy paste memes that were created by accident, and started spreading like wildfire. If you really are concerned, and want to see if this is true, check out `@discordapp` on Twitter, that is the official Twitter account of Discord, if there are urgent news, it will be tweeted there, and never announced through a generic copy pasta. If you’re unsure, feel free to ask a moderator. (Also, if you know of any fake ones, feel free to report them to <@140340499935395841>, he keeps track of them all)


```fix
A bot tagged me and told me to watch my language, what does that mean?
```
You’ve likely met <@140340499935395841>’s automated anti copy paste spam filter, this is here to prevent fake messages, such as the ones referred to above. If you feel like this has been a mistake, feel free to contact a moderator, or <@140340499935395841> directly, to see what the issue is.


```fix
Why was (insert person) put on warning?
```
As a rule, moderators do not discuss the warnings of others with anyone other than that person specifically, this is to prevent others getting involved and creating a larger issue than necessary.