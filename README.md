# Server Resources #

### Rezruel's one stop shop for resources about several servers that *can* be publically available for editing. ####

# What is this for? #
The purpose behind this repository is to allow others to comment, suggest changes, or complain
about issues with the current resources in the repo.
If you have any suggestions please leave them in a pull request.

How to pull request? Go to a file, edit it, and submit it back.
Remember to keep it to Discord formatting.